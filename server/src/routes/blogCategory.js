import express from "express"
import controller from "../controllers/blogCategory"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()

router.get("/", controller.getAllBlogCategories)


router.use(verifyToken)
router.post("/", isAdmin, controller.createBlogCategory)
router.put("/:id", isAdmin, controller.updateBlogCategory)
router.delete("/:id", isAdmin, controller.deleteBlogCategory)



module.exports = router