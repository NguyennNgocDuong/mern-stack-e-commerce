import express from "express"
import { upload } from "../config/configUploadFile"
import controller from "../controllers/blog"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()


router.get('/', controller.getAllBlogs)
router.get('/current-blog/:id', controller.getCurrentBlog)

router.use(verifyToken)
router.put('/like-blog/:id', controller.likeBlog)
router.put('/dislike-blog/:id', controller.disLikeBlog)
router.post('/', upload.array("images", 10), controller.createBlog)
router.delete('/:id', controller.deleteBlog)
router.put('/:id', upload.array("images", 10), controller.updateBlog)
router.put('/upload-image/:id', isAdmin, controller.uploadImageBlog)

module.exports = router
