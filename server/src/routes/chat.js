import express from "express"
import controller from "../controllers/chat"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()



router.use(verifyToken)
router.post("/", controller.createChat)
router.get("/", controller.userChats)
router.post("/send-message", controller.sendMessage)
router.get("/get-message/:chatId", controller.getMessage)




module.exports = router