import express from "express"
import { upload } from "../config/configUploadFile"
import controller from "../controllers/product"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()


router.get("/get-all-products", controller.getAllProducts)
router.get("/get-product/:id", controller.getProduct)

router.use(verifyToken)
router.put("/rating-product", controller.ratingProduct)
router.get("/feedback", controller.getFeedbacks)
router.post("/create-product", [isAdmin, upload.single("thump")], controller.createProduct)
router.put("/update-product/:id", [isAdmin, upload.single("thump")], controller.updateProduct)
router.delete("/delete-product/:id", isAdmin, controller.deleteProduct)
// router.put("/upload-image/:id", isAdmin, fileUploader.array('images', 5), controller.uploadImageProduct)


module.exports = router