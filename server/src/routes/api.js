import userRouter from "./user"
import productRouter from "./product"
import productCategoryRouter from "./productCategory"
import blogCategoryRouter from "./blogCategory"
import blogRouter from "./blog"
import brandRouter from "./brand"
import couponRouter from "./coupon"
import orderRouter from "./order"
import chatRouter from "./chat"
import { notFound, errHandler } from '../middlewares/errorHandler'

const initRoutes = (app) => {
    app.use("/api/user", userRouter)
    app.use("/api/product", productRouter)
    app.use("/api/product-category", productCategoryRouter)
    app.use("/api/blog-category", blogCategoryRouter)
    app.use("/api/blog", blogRouter)
    app.use("/api/brand", brandRouter)
    app.use("/api/coupon", couponRouter)
    app.use("/api/order", orderRouter)
    app.use("/api/chat", chatRouter)

    app.use(notFound)
    app.use(errHandler)
}

module.exports = initRoutes