import express from "express"
import controller from "../controllers/order"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()



router.use(verifyToken)
router.get("/get-order-user", controller.getOrderUser)
router.post("/", controller.createOrder)
router.put("/cancel-order/:id", controller.cancelOrder)
router.put("/:id", isAdmin, controller.updateOrder)
router.put("/update-status/:id", isAdmin, controller.updateStatus)
router.get("/", isAdmin, controller.getAllOrders)



module.exports = router