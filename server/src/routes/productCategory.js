import express from "express"
import { upload } from "../config/configUploadFile"
import controller from "../controllers/productCategory"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()

router.get("/", controller.getAllProductCategories)


router.use(verifyToken)
router.post("/", [isAdmin, upload.single("image")], controller.createProductCategory)
router.put("/:id", [isAdmin, upload.single("image")], controller.updateProductCategory)
router.delete("/:id", isAdmin, controller.deleteProductCategory)

router.post("/upload-category", [isAdmin, upload.single("image")], controller.uploadImageCategory)




module.exports = router