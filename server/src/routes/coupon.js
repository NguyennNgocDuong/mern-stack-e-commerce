import express from "express"
import controller from "../controllers/coupon"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()

router.get("/", controller.getAllCoupons)
router.get("/get-coupon/:name", controller.getCoupon)


router.use(verifyToken)
router.post("/", isAdmin, controller.createCoupon)
router.put("/:id", isAdmin, controller.updateCoupon)
router.delete("/:id", isAdmin, controller.deleteCoupon)



module.exports = router