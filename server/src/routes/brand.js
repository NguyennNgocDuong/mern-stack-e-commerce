import express from "express"
import controller from "../controllers/brand"
import { verifyToken, isAdmin } from "../middlewares/verifyToken"
const router = express.Router()

router.get("/", controller.getAllBrands)


router.use(verifyToken)
router.post("/", isAdmin, controller.createBrand)
router.put("/:id", isAdmin, controller.updateBrand)
router.delete("/:id", isAdmin, controller.deleteBrand)



module.exports = router