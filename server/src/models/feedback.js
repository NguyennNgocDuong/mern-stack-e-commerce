const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var feedbackSchema = new mongoose.Schema({
    product: { type: mongoose.Types.ObjectId, ref: "Product" },
    star: { type: Number },
    postedBy: { type: mongoose.Types.ObjectId, ref: "User" },
    comment: { type: String }



}, {
    timestamps: true,
});

//Export the model
module.exports = mongoose.model('Feedback', feedbackSchema);