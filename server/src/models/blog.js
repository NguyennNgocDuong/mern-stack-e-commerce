const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var blogSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,

    },
    images: {
        type: Array,
        default: []

    },
    author: { type: mongoose.Types.ObjectId, ref: "User" },
    comments: [{ type: mongoose.Types.ObjectId, ref: "Comment" }],
    views: {
        type: Number,
        default: 0,

    },
    likes: [
        { type: mongoose.Types.ObjectId, ref: "User" }
    ],
    disLikes: [
        { type: mongoose.Types.ObjectId, ref: "User" }
    ],
    isCensor: { type: Boolean, default: false }

}, { timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } });

//Export the model
module.exports = mongoose.model('Blog', blogSchema);