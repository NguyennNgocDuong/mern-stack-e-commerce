const mongoose = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var orderSchema = new mongoose.Schema({
    products: [{
        product: { type: mongoose.Types.ObjectId, ref: "Product" },
        quantity: { type: Number },
        color: { type: String },
    }],
    status: {
        type: String,
        default: "processing",
        enum: ["canceled", "processing", "delivered"]
    },
    paymentMethod: { type: String },
    address: {
        fullName: { type: String },
        email: { type: String },
        mobile: { type: String },
        province: { id: { type: Number }, name: { type: String } },
        district: { id: { type: Number }, name: { type: String } },
        ward: { id: { type: Number }, name: { type: String } },
        street: { type: String },
    },
    total: { type: Number, default: 0 },
    coupon: {
        type: mongoose.Types.ObjectId, ref: "Coupon"
    },
    orderBy: {
        type: mongoose.Types.ObjectId, ref: "User"
    }

}, {
    timestamps: true,
});

//Export the model
module.exports = mongoose.model('Order', orderSchema);