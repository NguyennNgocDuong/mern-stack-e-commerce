import Product from "../models/product"
import Feedback from "../models/feedback"
import asyncHandler from "express-async-handler"

import slugtify from "slugify"

const createProduct = asyncHandler(async (req, res) => {
    if (req.body && req.body.title) req.body.slug = slugtify(req.body.title)
    const { path } = req.file
    const { color } = req.body
    const colors = color.split(",")

    const newProduct = await Product.create({ ...req.body, thump: path, color: colors })
    return res.status(200).json({
        success: newProduct ? true : false,
        msg: newProduct ? "Add product successfully!" : "Product is existed!"
    })
})
const getProduct = asyncHandler(async (req, res) => {
    const { id } = req.params
    const product = await Product.findById(id).populate("ratings.postedBy", "firstName lastName image")
    return res.status(200).json({
        success: product ? true : false,
        product: product ? product : "Không tìm thấy sản phẩm"
    })
})
// fiter, sort,pagination
const getAllProducts = asyncHandler(async (req, res) => {
    const queries = { ...req.query }
    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)
    let orderObjectQuery = {}

    // Filltering
    if (queries?.title) formattedQuery.title = { $regex: queries.title, $options: "i" }
    if (queries?.category) formattedQuery.category = { $regex: queries.category, $options: "i" }
    if (queries?.color) {
        delete formattedQuery.color
        const arrColor = queries.color?.split(",")
        const colorQuery = arrColor.map(el => ({ color: { $regex: el, $options: "i" } }))
        orderObjectQuery = { $or: colorQuery }
    }
    if (queries?.brand) {
        delete formattedQuery.brand
        const arrBrand = queries.brand?.split(",")
        orderObjectQuery = { brand: { $in: arrBrand } }
    }
    const q = { ...orderObjectQuery, ...formattedQuery }

    let queryCommand = Product.find(q)

    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // Fields Limit
    if (req.query.fields) {
        const fields = req.query.fields.split(',').join(' ')
        queryCommand = queryCommand.select(fields)
    }

    // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 10
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await Product.find(q).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            products: response ? response : "Không tìm thấy sản phẩm",
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })

})
const updateProduct = asyncHandler(async (req, res) => {
    const { id } = req.params
    if (req.body && req.body.title) req.body.slug = slugtify(req.body.title)
    const { color } = req.body
    const colors = color.split(",")

    const product = await Product.findByIdAndUpdate(id, { ...req.body, thump: req.file?.path, color: colors }, { new: true })
    return res.status(200).json({
        success: product ? true : false,
        msg: product ? "Update product is successful!" : "Update product is failed!"
    })
})
const deleteProduct = asyncHandler(async (req, res) => {
    const { id } = req.params


    const product = await Product.findByIdAndDelete(id)
    return res.status(200).json({
        success: product ? true : false,
        msg: product ? "Xóa sản phẩm thành công!" : "Xóa sản phẩm thất bai!"
    })
})


// const ratingProduct = asyncHandler(async (req, res) => {
//     const { _id } = req.user
//     const { star, comment, pid } = req.body

//     const product = await Product.findById(pid)
//     if (product) {
//         // Tìm xem user đã đánh giá sản phẩm đó hay chưa
//         const alreadyRating = product.ratings.find(el => el.postedBy.toString() === _id)
//         if (alreadyRating) {
//             // nếu có thì cập nhật star & comment
//             await Product.updateOne({
//                 ratings: { $elemMatch: alreadyRating },
//             }, {
//                 $set: { "ratings.$.star": star, "ratings.$.comment": comment }
//             }, { new: true })

//         } else {
//             // nếu chưa thì thêm star & comment
//             await Product.findByIdAndUpdate(pid, {
//                 $push: { ratings: { star, comment, postedBy: _id } }
//             }, { new: true })

//         }

//         // Tổng lượt đánh giá
//         const updatedProduct = await Product.findById(pid)
//         const ratingCount = updatedProduct.ratings.length
//         const sumRatings = updatedProduct.ratings.reduce((sum, rating) => sum + rating.star, 0)
//         updatedProduct.totalRatings = Math.round(sumRatings * 10 / ratingCount) / 10
//         await updatedProduct.save()

//         return res.status(200).json({
//             success: true,
//             msg: "Thanks for your feedback!"

//         })
//     }

// })
const ratingProduct = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { star, comment, pid } = req.body

    // Tìm xem user đã đánh giá sản phẩm đó hay chưa
    const alreadyRating = await Feedback.findOne({ product: pid, postedBy: _id })

    if (alreadyRating) {
        // nếu có thì cập nhật star & comment
        await Feedback.updateOne(alreadyRating, {
            $set: { "star": star, "comment": comment }
        }, { new: true })

    } else {
        // nếu chưa thì thêm star & comment
        await Feedback.create({ ...req.body, product: pid, postedBy: _id })

    }

    // Tổng lượt đánh giá
    const feedback = await Feedback.find({ product: pid })
    const updatedProduct = await Product.findById(pid)
    const ratingCount = feedback.length
    const sumRatings = feedback.reduce((sum, rating) => sum + rating.star, 0)
    updatedProduct.totalRatings = Math.round(sumRatings * 10 / ratingCount) / 10
    await updatedProduct.save()

    return res.status(200).json({
        success: true,
        msg: "Thanks for your feedback!"

    })


})

const getFeedbacks = asyncHandler(async (req, res) => {
    const queries = { ...req.query }

    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)


    let queryCommand = Feedback.find(formattedQuery).populate("postedBy", "image firstName lastName")


    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // Fields Limit
    if (req.query.fields) {
        const fields = req.query.fields.split(',').join(' ')
        queryCommand = queryCommand.select(fields)
    }

    // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 10
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await Feedback.find(formattedQuery).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            feedbacks: response ? response : "Not found",
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })


})

const uploadImageProduct = asyncHandler(async (req, res) => {
    const { id } = req.params
    const product = await Product.findByIdAndUpdate(id, { $push: { images: { $each: req.files.map(el => el.path) } } }, { new: true })

    return res.status(200).json({
        success: product ? true : false,
        msg: product ? "Upload image successfully!" : "Cannot upload image!"

    })

})

module.exports = { createProduct, getProduct, getAllProducts, updateProduct, deleteProduct, ratingProduct, uploadImageProduct, getFeedbacks }