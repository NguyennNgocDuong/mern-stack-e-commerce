import BlogCategory from "../models/blogCategory"
import asyncHandler from "express-async-handler"

const createBlogCategory = asyncHandler(async (req, res) => {
    await BlogCategory.create(req.body)

    return res.status(200).json({
        success: true,
        msg: "Tạo danh mục thành công!"
    })
})
const getAllBlogCategories = asyncHandler(async (req, res) => {
    const categories = await BlogCategory.find()

    return res.status(200).json({
        success: categories ? true : false,
        categories

    })
})
const updateBlogCategory = asyncHandler(async (req, res) => {
    const { id } = req.params
    const category = await BlogCategory.findByIdAndUpdate(id, req.body, { new: true })

    return res.status(200).json({
        success: category ? true : false,
        msg: category ? "Cập nhật danh mục thành công!" : "Cập nhật danh mục thất bại!"
    })
})
const deleteBlogCategory = asyncHandler(async (req, res) => {
    const { id } = req.params
    const category = await BlogCategory.findByIdAndDelete(id)

    return res.status(200).json({
        success: category ? true : false,
        msg: category ? "Xóa danh mục thành công!" : "Xóa danh mục thất bại!"
    })
})

module.exports = { createBlogCategory, getAllBlogCategories, updateBlogCategory, deleteBlogCategory }