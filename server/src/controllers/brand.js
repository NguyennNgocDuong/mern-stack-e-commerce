import Brand from "../models/brand"
import asyncHandler from "express-async-handler"

const createBrand = asyncHandler(async (req, res) => {
    await Brand.create(req.body)

    return res.status(200).json({
        success: true,
        msg: "Tạo nhãn hàng thành công!"
    })
})
const getAllBrands = asyncHandler(async (req, res) => {
    const brands = await Brand.find()

    return res.status(200).json({
        success: brands ? true : false,
        brands

    })
})
const updateBrand = asyncHandler(async (req, res) => {
    const { id } = req.params
    const brand = await Brand.findByIdAndUpdate(id, req.body, { new: true })

    return res.status(200).json({
        success: brand ? true : false,
        msg: brand ? "Cập nhật nhãn hàng thành công!" : "Cập nhật nhãn hàng thất bại!"
    })
})
const deleteBrand = asyncHandler(async (req, res) => {
    const { id } = req.params
    const brand = await Brand.findByIdAndDelete(id)

    return res.status(200).json({
        success: brand ? true : false,
        msg: brand ? "Xóa nhãn hàng thành công!" : "Xóa nhãn hàng thất bại!"
    })
})

module.exports = { createBrand, getAllBrands, updateBrand, deleteBrand }