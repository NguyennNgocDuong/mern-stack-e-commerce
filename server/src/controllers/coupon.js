import Coupon from "../models/coupon"
import asyncHandler from "express-async-handler"

const createCoupon = asyncHandler(async (req, res) => {
    await Coupon.create({ ...req.body, expire: Date.now() + req.body.expire * 1 * 24 * 60 * 60 * 1000 })

    return res.status(200).json({
        success: true,
        msg: "Tạo coupon thành công!"
    })
})
const getAllCoupons = asyncHandler(async (req, res) => {
    const coupons = await Coupon.find()

    return res.status(200).json({
        success: coupons ? true : false,
        coupons

    })
})
const getCoupon = asyncHandler(async (req, res) => {
    const { name } = req.params

    const coupon = await Coupon.findOne({ name })

    if (coupon) {

        if (Date.parse(coupon.expire) < Date.now()) {
            return res.status(200).json({
                success: false,
                msg: "Coupon is expired!"
            })
        } else {
            return res.status(200).json({
                success: true,
                msg: "Apply coupon successful!",
                coupon
            })
        }


    } else {
        return res.status(200).json({
            success: false,
            msg: "Coupon is invalid!"
        })
    }



})



const updateCoupon = asyncHandler(async (req, res) => {
    const { id } = req.params
    const coupon = await Coupon.findByIdAndUpdate(id, { ...req.body, expire: Date.now() + req.body.expire * 1 * 24 * 60 * 60 * 1000 }, { new: true })

    return res.status(200).json({
        success: coupon ? true : false,
        msg: coupon ? "Cập nhật coupon thành công!" : "Cập nhật coupon thất bại!"
    })
})
const deleteCoupon = asyncHandler(async (req, res) => {
    const { id } = req.params
    const coupon = await Coupon.findByIdAndDelete(id)

    return res.status(200).json({
        success: coupon ? true : false,
        msg: coupon ? "Xóa coupon thành công!" : "Xóa coupon thất bại!"
    })
})

module.exports = { createCoupon, getAllCoupons, updateCoupon, deleteCoupon, getCoupon }