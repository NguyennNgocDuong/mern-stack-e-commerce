import ProductCategory from "../models/productCategory"
import asyncHandler from "express-async-handler"

const createProductCategory = asyncHandler(async (req, res) => {
    const { brand, title } = req.body

    const { path } = req.file

    const brands = brand.split(",")
    await ProductCategory.create({ title, brand: brands, image: path })

    return res.status(200).json({
        success: true,
        msg: "Tạo danh mục thành công!"
    })
})

const uploadImageCategory = asyncHandler(async (req, res) => {
    if (!req.file) {
        throw new Error('No file uploaded!')
    }

    res.json({ secure_url: req.file.path });
    // const { id } = req.params
    // const product = await Product.findByIdAndUpdate(id, { $push: { images: { $each: req.files.map(el => el.path) } } }, { new: true })

    // return res.status(200).json({
    //     success: product ? true : false,
    //     msg: product ? "Upload image successfully!" : "Cannot upload image!"

    // })
    return res.status(200).json({
        success: true,
        msg: "Upload image successfully!"

    })

})

const getAllProductCategories = asyncHandler(async (req, res) => {
    const queries = { ...req.query }
    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)



    let queryCommand = ProductCategory.find(formattedQuery)

    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // Fields Limit
    if (req.query.fields) {
        const fields = req.query.fields.split(',').join(' ')
        queryCommand = queryCommand.select(fields)
    }

    // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 10
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await ProductCategory.find(formattedQuery).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            categories: response ? response : "Category not found!",
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })

})
const updateProductCategory = asyncHandler(async (req, res) => {
    const { brand, title } = req.body

    const brands = brand.split(",")
    const { id } = req.params
    const category = await ProductCategory.findByIdAndUpdate(id, { title, brand: brands, image: req.file?.path }, { new: true })

    return res.status(200).json({
        success: category ? true : false,
        msg: category ? "Cập nhật danh mục thành công!" : "Cập nhật danh mục thất bại!"
    })
})
const deleteProductCategory = asyncHandler(async (req, res) => {
    const { id } = req.params
    const category = await ProductCategory.findByIdAndDelete(id)

    return res.status(200).json({
        success: category ? true : false,
        msg: category ? "Xóa danh mục thành công!" : "Xóa danh mục thất bại!"
    })
})

module.exports = { createProductCategory, getAllProductCategories, updateProductCategory, deleteProductCategory, uploadImageCategory }