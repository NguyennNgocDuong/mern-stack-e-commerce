import Chat from "../models/chat"
import Message from "../models/message"
import asyncHandler from "express-async-handler"

// POST [chat/]
const createChat = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { receiveredId } = req.body

    if (receiveredId === _id) throw new Error("Somthing wrong")

    const chat = await Chat.findOne({
        $or: [
            { members: [_id, receiveredId] },
            { members: [receiveredId, _id] }
        ]
    })
    if (!chat) {

        const newChat = await Chat.create({ members: [_id, receiveredId] })
        return res.status(200).json({
            success: newChat ? true : false,
        })
    }

    return res.status(200).json({
        success: true,
    })




})

// GET [chat/]
const userChats = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const chat = await Chat.find({ members: { $in: [_id] } })

    return res.status(200).json({
        success: chat ? true : false,
        data: chat ? chat : "failed"
    })
})

// POST [chat/send-message]
const sendMessage = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const newMessage = await Message.create({ ...req.body, senderId: _id })
    return res.status(200).json({
        success: newMessage ? true : false,
    })
})

// GET [chat/get-message/:chatId]
const getMessage = asyncHandler(async (req, res) => {
    const { chatId } = req.params
    const message = await Message.find({ chatId })
    return res.status(200).json({
        success: message ? true : false,
        message
    })
})




module.exports = { createChat, userChats, sendMessage, getMessage }