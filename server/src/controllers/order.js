import Order from "../models/order"
import User from "../models/user"
import Coupon from "../models/coupon"
import asyncHandler from "express-async-handler"
import Product from "../models/product"

const createOrder = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { coupon, paymentMethod, address } = req.body
    const userCart = await User.findById(_id).select("cart").populate("cart.product", "title price")
    const promise = userCart?.cart.map(async (el) => {
        await Product.findOneAndUpdate(
            {
                _id: el.product._id,
                quantity: { $gte: el.quantity }
            },
            {
                $inc: {
                    quantity: -el.quantity,
                    sold: +el.quantity
                }
            },
            { new: true }
        )


        return {
            product: el.product._id,
            quantity: el.quantity,
            color: el.color
        }
    })

    const products = await Promise.all(promise)

    let total = userCart.cart.reduce((sum, el) => el.product.price * el.quantity + sum, 0)
    if (coupon) {
        const rs = await Coupon.findById(coupon).select("discount")
        if (rs) total = Math.round(total * (1 - rs.discount * 1 / 100) * 100) / 100
    }


    const rs = await Order.create({ products, total, orderBy: _id, coupon, paymentMethod, address })
    if (rs) {
        await User.findByIdAndUpdate(_id, { $set: { "cart": [] } }, { new: true })
        return res.status(200).json({
            success: rs ? true : false,
            msg: rs ? "Order successful!" : "Order failed!",
            rs
        })
    }

})


const getAllOrders = asyncHandler(async (req, res) => {
    const queries = { ...req.query }
    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)

    // Filltering
    if (queries?.status) formattedQuery.status = { $regex: queries.status, $options: "i" }

    let queryCommand = Order.find({ ...formattedQuery }).populate("products.product", "title thump price category")

    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // // Fields Limit
    // if (req.query.fields) {
    //     const fields = req.query.fields.split(',').join(' ')
    //     queryCommand = queryCommand.select(fields)
    // }

    // // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 5
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await Order.find({ ...formattedQuery }).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            orders: response,
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })
})
const getOrderUser = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const queries = { ...req.query }
    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)

    // Filltering
    if (queries?.status) formattedQuery.status = { $regex: queries.status, $options: "i" }

    let queryCommand = Order.find({ ...formattedQuery, orderBy: _id }).populate("products.product", "title thump price category")

    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // // Fields Limit
    // if (req.query.fields) {
    //     const fields = req.query.fields.split(',').join(' ')
    //     queryCommand = queryCommand.select(fields)
    // }

    // // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 5
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await Order.find({ ...formattedQuery, orderBy: _id }).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            orders: response,
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })
})



const cancelOrder = asyncHandler(async (req, res) => {
    const { id } = req.params
    const order = await Order.findByIdAndUpdate(id, { $set: { "status": "canceled" } }, { new: true })

    return res.status(200).json({
        success: order ? true : false,
        msg: order ? "Cancel order successfully!" : "Cancel order failed!"
    })
})
const updateStatus = asyncHandler(async (req, res) => {
    const { id } = req.params
    const order = await Order.findByIdAndUpdate(id, { $set: { "status": "delivered" } }, { new: true })

    return res.status(200).json({
        success: order ? true : false,
        msg: order ? "Update status successfully!" : "Update status failed!"

    })
})
const updateOrder = asyncHandler(async (req, res) => {
    const { id } = req.params
    const order = await Order.findByIdAndUpdate(id, req.body, { new: true })

    return res.status(200).json({
        success: order ? true : false,
        msg: order ? "Cập nhật đơn hàng thành công!" : "Cập nhật đơn hàng thất bại!"
    })
})


module.exports = { createOrder, updateStatus, getOrderUser, getAllOrders, updateOrder, cancelOrder }