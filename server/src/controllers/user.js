import User from '../models/user'
import asyncHandler from "express-async-handler"
import jwt from "jsonwebtoken"
import crypto from 'crypto';

import { generateAccessToken, generateRefreshToken } from "../middlewares/jwt"
import { sendMail } from "../utils/sendMail"

const register = asyncHandler(async (req, res) => {
    const { email, password, firstName, lastName } = req.body
    const otp = req.otp


    if (!email || !password || !firstName || !lastName) {
        return res.status(400).json({
            success: false,
            msg: "Missing input!"
        })
    }

    const alreadyUser = await User.findOne({ email })

    if (alreadyUser) throw new Error("User is existed!")

    await User.create({ email, password, firstName, lastName, otp })
    const html = `Đây là mã OTP xác thực cho địa chỉ e-mail của bạn:
    <b>${otp}</b>. Mã có hiệu lực trong vòng 1 phút!`

    const data = {
        email, html, subject: "Verify email"
    }

    await sendMail(data)
    return res.status(200).json({
        success: true,
        msg: "Please check your email to verify!"
    })

})

const verifyEmailRegister = asyncHandler(async (req, res) => {
    const { email, otp } = req.body

    const user = await User.findOne({ email, otp })

    if (!user) {
        await User.deleteOne({ email });
        throw new Error("Invalid otp! Please sign up again")
    }
    user.otp = undefined
    await user.save()
    return res.status(200).json({
        success: true,
        msg: "Register successfully!"
    })


})


const login = asyncHandler(async (req, res) => {
    const { email, password } = req.body

    if (!email || !password) {
        return res.status(400).json({
            success: false,
            msg: "Missing input!"
        })
    }

    const response = await User.findOne({ email })
    if (response && await response.isCorrectPassword(password)) {
        const { password, role, refreshToken, ...userData } = response.toObject()
        const accessToken = generateAccessToken(response._id, role)
        const newRefreshToken = generateRefreshToken(response._id)

        // lưu refreshToken vào model User
        await User.findByIdAndUpdate(response._id, { refreshToken: newRefreshToken }, { new: true })

        // lưu refreshToken vào cookie
        res.cookie("refreshToken", newRefreshToken, { httpOnly: true, maxAge: 7 * 24 * 60 * 60 * 1000 })

        return res.status(200).json({
            success: true,
            msg: "Login is successfully!",
            accessToken,
            userData
        })
    } else {
        throw new Error("Email isn't exist or password is incorrect!")
    }



})


const refreshToken = asyncHandler(async (req, res) => {
    // lấy refresh token dưới cookie
    const cookie = req.cookies
    // nếu không có token thì báo lỗi
    if (!cookie && !cookie.refreshToken) throw new Error("No refresh token in cookie!")
    // nếu có token thì xác thực token
    const result = await jwt.verify(cookie.refreshToken, process.env.JWT_SECRET)
    const response = await User.findOne({ _id: result._id, refreshToken: cookie.refreshToken })
    return res.status(200).json({
        success: response ? true : false,
        newAccessToken: response ? generateAccessToken(response._id, response.role) : "Refresh Token not match!"
    })

})

const logout = asyncHandler(async (req, res) => {
    // lấy refresh token dưới cookie
    const cookie = req.cookies
    // nếu không có token thì báo lỗi
    if (!cookie && !cookie.refreshToken) throw new Error("No refresh token in cookie!")
    //    Xóa refeshToken ở DB
    await User.findOneAndUpdate({ refreshToken: cookie.refreshToken }, { refreshToken: "" }, { new: true })
    // Xóa refreshToken ở cookie
    res.clearCookie("refreshToken", {
        httpOnly: true,
        secure: true
    })
    return res.status(200).json({
        success: true,
        msg: "Logout is successfully!"
    })

})

const forgotPassword = asyncHandler(async (req, res) => {
    const { email } = req.body
    const otp = req.otp


    if (!email) {
        return res.status(400).json({
            success: false,
            msg: "Missing input!"
        })
    }

    const alreadyUser = await User.findOne({ email })

    if (!alreadyUser) throw new Error("Email is not existed!")

    alreadyUser.otp = otp
    await alreadyUser.save()
    const html = `Đây là mã OTP xác thực cho địa chỉ e-mail của bạn:
    <b>${otp}</b>. Mã có hiệu lực trong vòng 1 phút!`

    const data = {
        email, html, subject: "Reset Password"
    }

    await sendMail(data)
    return res.status(200).json({
        success: true,
        msg: "Please check your email to verify!"
    })

})

const verifyOTPResetPassword = asyncHandler(async (req, res) => {
    const { email, otp } = req.body

    const user = await User.findOne({ email, otp })


    if (!user) {
        const userUpdate = await User.findOne({ email })
        userUpdate.otp = undefined
        await userUpdate.save()
        throw new Error("Invalid otp! Please try again")
    }
    user.otp = undefined
    await user.save()
    return res.status(200).json({
        success: true,
        msg: "Verify successful!"
    })


})


const resetPassword = asyncHandler(async (req, res) => {
    const { password, email } = req.body

    if (!password) {
        return res.status(400).json({
            success: false,
            msg: "Missing input!"
        })
    }

    const user = await User.findOne({ email })
    if (!user) throw new Error("Please try again")
    user.password = password
    await user.save()

    return res.status(200).json({
        success: true,
        msg: "Password is updated!"
    })


})

const getCurrentUser = asyncHandler(async (req, res) => {

    const { _id } = req.user
    const user = await User.findById({ _id }).select("-refreshToken -password").populate("cart.product", "title brand category thump price quantity").populate("wishList", "thump title category totalRatings price")
    return res.status(200).json({
        success: user ? true : false,
        response: user ? user : "User not found!",
    })


})

// GET [user/get-user/:id]
const getUserId = asyncHandler(async (req, res) => {

    const { id } = req.params
    const user = await User.findById({ _id: id }).select("firstName lastName role image")
    return res.status(200).json({
        success: user ? true : false,
        response: user ? user : "User not found!",
    })


})

const getAllUsers = asyncHandler(async (req, res) => {
    const queries = { ...req.query }
    // Tách các trường đặc biệt ra khỏi query
    const excludeFields = ["limit", "sort", "page", "fields"]
    excludeFields.forEach(el => delete queries[el])

    // Format lại operators cho đúng cú pháp mongoose
    const queryString = JSON.stringify(queries).replace(/\b(gte|gt|lt|lte)\b/g, el => `$${el}`)
    const formattedQuery = JSON.parse(queryString)



    let queryCommand = User.find(formattedQuery)

    // Sorting
    if (req.query.sort) {
        const sortBy = req.query.sort.split(',').join(' ')
        queryCommand = queryCommand.sort(sortBy)
    }

    // Fields Limit
    if (req.query.fields) {
        const fields = req.query.fields.split(',').join(' ')
        queryCommand = queryCommand.select(fields)
    }

    // Pagination
    const page = req.query.page * 1 || 1
    const limit = req.query.limit * 1 || 10
    const skip = (page - 1) * limit
    queryCommand.skip(skip).limit(limit)

    queryCommand.then(async (response) => {
        const count = await User.find(formattedQuery).countDocuments()
        return res.status(200).json({
            success: response ? true : false,
            count,
            users: response ? response : "Không tìm thấy user",
        })
    })
        .catch(err => {
            throw new Error(err.message)

        })


})
const deleteUser = asyncHandler(async (req, res) => {
    const { id } = req.params
    if (!id) throw new Error("Missing input!")
    const user = await User.findByIdAndDelete(id)
    return res.status(200).json({
        success: user ? true : false,
        msg: user ? `${user.email} is deleted!` : "No user delete!"
    })


})

const updateUser = asyncHandler(async (req, res) => {
    const { _id } = req.user
    // if (!_id || !req.body) throw new Error("Missing input!")
    const user = await User.findByIdAndUpdate(_id, { ...req.body, image: req.file?.path }, { new: true })
    return res.status(200).json({
        success: user ? true : false,
        msg: user ? `Update successful!` : "Error updating!",
    })


})
const updateUserByAdmin = asyncHandler(async (req, res) => {
    const { uid } = req.params

    // if (!_id || !req.body) throw new Error("Missing input!")
    const user = await User.findByIdAndUpdate(uid, req.body, { new: true }).select("-refreshToken -role -password")
    return res.status(200).json({
        success: user ? true : false,
        msg: user ? `Update successful!` : "Error updating!",
        user
    })


})
// const updateUserAddress = asyncHandler(async (req, res) => {
//     const { _id } = req.user
//     const user = await User.findByIdAndUpdate(_id, { address: req.body.address }, { new: true }).select("address")
//     return res.status(200).json({
//         success: user ? true : false,
//         msg: user ? `Update successful!` : "Error updating!",
//         user
//     })


// })
const addToCart = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id, color, quantity } = req.body

    const user = await User.findById(_id).select("cart")
    if (user) {
        // Tìm xem sản phẩm đã có trong giỏ hàng chưa
        const alreadyProduct = user.cart.find(el => el.product.toString() === id && el.color.toString() === color)
        if (alreadyProduct) {
            // Nếu có thì check xem sản phẩm có trùng màu hay không
            if (alreadyProduct.color === color) {
                // Nếu có thì cập nhật quantity
                await User.updateOne({
                    cart: { $elemMatch: alreadyProduct }
                }, {
                    // $set: { "cart.$.quantity": quantity }
                    $inc: { "cart.$.quantity": quantity }
                }, { new: true })
            } else {
                // Neus không thì  thêm sản phẩm vào giỏ hàng
                await User.findByIdAndUpdate(_id, { $push: { cart: { product: id, color, quantity } } }, { new: true })
            }
        } else {
            // Nếu chưa thì thêm sản phẩm vào giỏ hàng
            await User.findByIdAndUpdate(_id, { $push: { cart: { product: id, color, quantity } } }, { new: true })
        }
        return res.status(200).json({
            success: true,
            msg: "Thêm giỏ hàng thành công!"
        })
    }
})

const deleteCart = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id } = req.params

    const updateCart = await User.findByIdAndUpdate(_id, { $pull: { cart: { _id: id } } }, { new: true })
    return res.status(200).json({
        success: updateCart ? true : false,
        msg: updateCart ? "Delete successful!" : "wrong"
    })
})
const clearCart = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const updateCart = await User.findByIdAndUpdate(_id, { $set: { "cart": [] } }, { new: true })
    return res.status(200).json({
        success: updateCart ? true : false,
        msg: updateCart ? "Clear cart successful!" : "wrong"
    })
})


const changeQuantityCart = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { number, ...data } = req.body

    const user = await User.findById(_id).select("cart")
    if (user) {
        const alreadyProduct = user.cart.find(el => el.product.toString() === data?.product?._id && el.color.toString() === data?.color)
        if (alreadyProduct) {
            const updateCart = await User.updateOne({
                cart: { $elemMatch: alreadyProduct }
            }, {
                $set: { "cart.$.quantity": number }
            }, { new: true })
            return res.status(200).json({
                success: updateCart ? true : false,
            })
        }
    }
    return res.status(200).json({
        success: false,
    })

})

const addAddress = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const user = await User.findById(_id).select("address")
    if (user) {
        const isDefaultAddress = user.address.find((el) => el?.isDefault === true)
        if (isDefaultAddress) {
            await User.updateOne({
                address: { $elemMatch: isDefaultAddress }
            }, {
                $set: {
                    "address.$.isDefault": false,
                }
            }, { new: true })
        }
        const updateAddress = await User.findByIdAndUpdate(_id, { $push: { address: req.body } }, { new: true })

        return res.status(200).json({
            success: updateAddress ? true : false,
            msg: updateAddress ? "Add address succesful!" : "Add address failed!"
        })
    }
})

const deleteAddress = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id } = req.params

    const updateAddress = await User.findByIdAndUpdate(_id, { $pull: { address: { _id: id } } }, { new: true })

    return res.status(200).json({
        success: updateAddress ? true : false,
        msg: updateAddress ? "Delete address succesful!" : "Delete address failed!"
    })

})
const updateAddress = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id, fullName, mobile, email, province, district, ward, street } = req.body

    const user = await User.findById(_id).select("address")
    if (user) {
        const alreadyAddress = user.address.find((el) => el?._id.toString() === id)
        if (alreadyAddress) {
            const updateAddress = await User.updateOne({
                address: { $elemMatch: alreadyAddress }
            }, {
                $set: {
                    "address.$.fullName": fullName,
                    "address.$.mobile": mobile,
                    "address.$.email": email,
                    "address.$.province": province,
                    "address.$.district": district,
                    "address.$.ward": ward,
                    "address.$.street": street,
                }
            }, { new: true })
            return res.status(200).json({
                success: updateAddress ? true : false,
                msg: updateAddress ? "Update address successful!" : "Update address failed!"

            })
        }
    }
    return res.status(200).json({
        success: false,
        msg: "Update address failed!"
    })

})
const setDefaultAddress = asyncHandler(async (req, res) => {
    const { _id } = req.user

    const { id } = req.params


    const user = await User.findById(_id).select("address")
    if (user) {
        const isDefaultAddress = user.address.find((el) => el?.isDefault === true)
        if (isDefaultAddress) {
            await User.updateOne({
                address: { $elemMatch: isDefaultAddress }
            }, {
                $set: {
                    "address.$.isDefault": false,
                }
            }, { new: true })
        }

        const address = user.address.find((el) => el?._id.toString() === id)
        if (address) {
            await User.updateOne({
                address: { $elemMatch: address }
            }, {
                $set: {
                    "address.$.isDefault": true,
                }
            }, { new: true })
        }
        return res.status(200).json({
            success: true,
        })

    }
    return res.status(200).json({
        success: false,
    })

})

const addWishList = asyncHandler(async (req, res) => {
    const { _id } = req.user
    const { id } = req.params

    const user = await User.findById(_id).select("wishList")

    if (user) {
        // Kiểm tra xem user đã add wish list chưa
        const alreadyAdd = user?.wishList.find(el => el.toString() === id)
        if (alreadyAdd) {
            // Nếu rồi thì xóa
            await User.findByIdAndUpdate(_id, { $pull: { wishList: id } }, { new: true })
        } else {
            // Nếu chưa thì thêm
            await User.findByIdAndUpdate(_id, { $push: { wishList: id } }, { new: true })

        }

        return res.status(200).json({
            success: true

        })
    }
    return res.status(200).json({
        success: false
    })

})


module.exports = {
    register, login, getCurrentUser, refreshToken, logout, forgotPassword, resetPassword, getAllUsers, deleteUser, updateUser, updateUserByAdmin, addToCart, verifyEmailRegister, verifyOTPResetPassword, deleteCart, changeQuantityCart, clearCart, addAddress, deleteAddress, updateAddress, setDefaultAddress, addWishList, getUserId
}