import { BsChatDots, BsHeart, BsCart2, BsHeartFill, BsThreeDots,BsBookmark } from "react-icons/bs";
import {
    MdLogout, MdShoppingCartCheckout, MdKeyboardArrowDown, MdFilterListAlt, MdManageAccounts, MdPerson, MdOutlineDashboard, MdHome, MdCloudUpload,
    MdDelete, MdReorder
} from "react-icons/md";
import { AiOutlineUser, AiFillEye, AiOutlineFacebook, AiOutlineGooglePlus, AiOutlineLinkedin, AiOutlineSend, AiFillPicture, AiFillCamera } from "react-icons/ai";
import { FiEdit, FiTrash2, FiPlus } from "react-icons/fi";
import { FcFeedback } from "react-icons/fc";
import { FaUserAlt, FaTruckMoving, FaLocationArrow, FaSearch, FaMicrophone, FaLinkedinIn, FaTimes } from "react-icons/fa";
import { BiLogoFacebook,BiComment } from "react-icons/bi";



export const icons = {
    BsChatDots, BsHeart, BsHeartFill, BsCart2, MdLogout, MdShoppingCartCheckout, MdKeyboardArrowDown, MdFilterListAlt, MdManageAccounts, MdPerson, MdOutlineDashboard, MdHome, AiOutlineUser, AiFillEye, FiEdit, FiTrash2, FiPlus, MdCloudUpload,
    MdDelete, MdReorder, FaUserAlt, FaTruckMoving, FaLocationArrow, FaSearch, FaMicrophone, FcFeedback, BiLogoFacebook, AiOutlineGooglePlus, FaLinkedinIn, AiOutlineSend, AiFillPicture, FaTimes, AiFillCamera, BsThreeDots,BiComment,BsBookmark
}