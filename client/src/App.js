import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Route, Routes } from 'react-router-dom';
import './App.css';
import { Login, Home, Public, Product, Blog, Contact, DetailProduct, Register, ForgotPassword, ResetPassword, NotFound, Cart, Checkout, DetailNews } from "./pages/public"
import { getAllCategories } from './redux/asyncAction';
import path from './routes/path';
import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import { Private, Profile, Order, WishList, Chat } from 'pages/private';
import { Admin, Dashboard, ManageBrand, ManageCategory, ManageProduct, ManageUser, ManageOrder } from 'pages/admin';
import { Loading } from 'components';


function App() {

  const { loading } = useSelector((state) => state.loadingSlice)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getAllCategories())
  }, [dispatch])
  return (
    <>
      <div className="min-h-screen font-main bg-main relative">
        <Loading isLoading={loading} fullscreen={true} />

        <ToastContainer
        />
        <Routes>
          <Route path={path.PUBLIC} element={<Public />}>
            <Route path={path.HOME} element={<Home />} />
            <Route path={path.PRODUCT} element={<Product />} />
            <Route path={path.BLOG} element={<Blog />} />
            <Route path={path.CONTACT} element={<Contact />} />
            <Route path={path.CART} element={<Cart />} />
            <Route path={path.CHECKOUT} element={<Checkout />} />
            <Route path={path.DETAIL_PRODUCT} element={<DetailProduct />} />
            <Route path={path.DETAIL_NEWS} element={<DetailNews />} />
          </Route>

          <Route path={path.PRIVATE} element={<Private />} >
            <Route path={path.PROFILE} element={<Profile />} />
            <Route path={path.ORDER} element={<Order />} />
            <Route path={path.WISHLIST} element={<WishList />} />
            <Route path={path.CHAT} element={<Chat />} />
          </Route>
          <Route path={path.ADMIN} element={<Admin />} >
            <Route path={path.DASHBOARD} element={<Dashboard />} />
            <Route path={path.MANAGEPRODUCT} element={<ManageProduct />} />
            <Route path={path.MANAGEUSER} element={<ManageUser />} />
            <Route path={path.MANAGECATEGORY} element={<ManageCategory />} />
            <Route path={path.MANAGEBRAND} element={<ManageBrand />} />
            <Route path={path.MANAGEORDER} element={<ManageOrder />} />
          </Route>

          <Route path={path.LOGIN} element={<Login />} />
          <Route path={path.REGISTER} element={<Register />} />
          <Route path={path.FORGOTPASSWORD} element={<ForgotPassword />} />
          <Route path={path.RESETPASSWORD} element={<ResetPassword />} />
          <Route path={path.ALL} element={<NotFound />} />


        </Routes>
      </div>
    </>
  );
}

export default App;
