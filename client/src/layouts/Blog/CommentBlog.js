import React, { useState } from 'react'
import InputEmoji from "react-input-emoji";
import { icons } from 'utils/icons';

const CommentBlog = () => {
    const [comment, setComment] = useState("")

    const { AiOutlineSend } = icons

    const handleComment = () => {
        if (!comment.trim()) return
        console.log('comment: ', comment);
    }

    return (
        <div>
            <div className=" w-full  flex items-center">

                <InputEmoji
                    value={comment}
                    onChange={(msg) => setComment(msg)}
                    cleanOnEnter
                    onEnter={handleComment}
                    placeholder="Comment...."
                />
                <button
                    onClick={handleComment}
                    className="bg-feature text-hover p-2 rounded-lg"
                >
                    <AiOutlineSend />
                </button>
            </div>
        </div>
    )
}

export default CommentBlog