import React, { memo, useEffect, useRef, useState } from "react";
import Avatar from "assets/img/avatar.png";
import { formatCreatedAt } from "utils/helpers";
import { Slider } from "components";
import { useDispatch, useSelector } from "react-redux";
import { icons } from "utils/icons";
import { blogService } from "services/blogService";
import { getAllPosts } from "redux/asyncAction";
import { toastSucess, toastError } from "utils/helpers";
import { Modal } from "antd";
import FormNews from "./FormNews";
import Interact from "./Interact";
import CommentBlog from "./CommentBlog";
import { chatService } from "services/chatService";
import { useNavigate } from "react-router-dom";
import path from "routes/path";

const NewsItem = ({ data }) => {
  // console.log('data: ', data?.images.length);
  const { currentUser } = useSelector((state) => state.userSlice);
  const [isSetting, setIsSetting] = useState(false);
  const [isModal, setIsModal] = useState(false);
  const { BsThreeDots, BsChatDots } = icons;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const menuRef = useRef();

  const handleCloseMenu = (e) => {
    if (!menuRef.current?.contains(e.target)) {
      setIsSetting(false);
    }
  };

  const handleDeleteNews = async (id) => {
    const rs = await blogService.handleDeleteNews(id);
    if (rs?.success) {
      toastSucess(rs?.msg);
      dispatch(getAllPosts({ isCensor: true }));
    } else {
      toastError(rs?.msg);
    }
  };

  const handleChat = async () => {
    const rs = await chatService.handleCreateChat({ receiveredId: data?.author?._id })
    if (rs?.success) {
      navigate(`/${path.PRIVATE}/${path.CHAT}`)
    }
  }

  useEffect(() => {
    document.addEventListener("mousedown", handleCloseMenu);

    return () => {
      document.removeEventListener("mousedown", handleCloseMenu);
    };
  });

  return (
    <div className="bg-sub px-3 py-5 rounded-lg mb-5 shadow-lg">
      <Modal
        title={<h1 className="text-2xl font-semibold">Create News</h1>}
        centered
        open={isModal}
        onCancel={() => setIsModal(false)}
        footer={null}
        width={500}
      >
        <FormNews type="update" setIsModal={setIsModal} data={data} />
      </Modal>
      <div className="flex justify-between ">
        <div className="flex items-center gap-3  ">
          <img
            className="w-[50px] shadow-md rounded-full"
            src={data?.author?.image ? `${data?.author?.image}` : Avatar}
            alt=""
          />
          <div className="text-sm relative">
            <h3 className="font-semibold cursor-pointer relative group">
              {data?.author?.firstName + "" + data?.author?.lastName}
              <div onClick={handleChat} className="absolute top-[20px] right-0 hidden group-hover:block transition duration-500 bg-main rounded-lg p-3 shadow-lg">
                <BsChatDots />
              </div>
            </h3>
            <span className="text-[12px] italic text-sub">
              {formatCreatedAt(data?.createdAt)}
            </span>

          </div>
        </div>
        {currentUser?._id === data?.author?._id && (
          <div
            ref={menuRef}
            onClick={() => setIsSetting((prev) => !prev)}
            className="cursor-pointer relative"
          >
            <BsThreeDots />
            {isSetting && (
              <div className="bg-sub shadow-2xl absolute left-[-70px] top-[20px] px-2 text-sm ">
                <p
                  onClick={() => setIsModal((prev) => !prev)}
                  className="p-2 hover:bg-main"
                >
                  Edit
                </p>
                <p
                  onClick={() => handleDeleteNews(data?._id)}
                  className="p-2 hover:bg-main"
                >
                  Remove
                </p>
              </div>
            )}
          </div>
        )}
      </div>
      <p className="my-3">{data?.content}</p>
      <Slider
        navigation
        className="object-cover w-full h-[700px]"
        images={data?.images}
      />

      <div className="border-t-2 pt-5">
        <Interact currentNews={data} />
      </div>

      {/* <div>
        <CommentBlog />
      </div> */}
    </div>
  );
};

export default memo(NewsItem);
