import React, { memo, useState, useRef } from "react";
import { icons } from "utils/icons";
import { Button, Loading } from "components";
import { toastError, toastSucess } from "utils/helpers";
import { blogService } from "services/blogService";
import { useDispatch } from "react-redux"
import { getAllPosts } from "redux/asyncAction";

const FormNews = ({ setIsModal, data, type }) => {
  const { AiFillPicture, FaTimes, AiFillCamera } = icons;
  const [content, setContent] = useState(data ? data.content : "");
  const [images, setImages] = useState(data ? data.images : []);

  const [isCamera, setIsCamera] = useState(false);
  const [tracks, setTracks] = useState("");
  const [loading, setLoading] = useState(false)
  const videoRef = useRef();
  const canvasRef = useRef();
  const dispatch = useDispatch()

  const handleChooseImage = (e) => {
    const files = [...e.target.files];
    const listImages = [];

    files.forEach((file) => {
      if (!file) {
        toastError("File does not exist!");
        return;
      }

      if (file.size > 1024 * 1024 * 5) {
        toastError("The image largest is 5mb.");
        return;
      }

      return listImages.push(file);
    });

    setImages((prev) => [...prev, ...listImages]);
  };


  const handleStream = () => {
    setIsCamera((prev) => !prev);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then((mediaStream) => {
          videoRef.current.srcObject = mediaStream;
          videoRef.current.play();

          const track = mediaStream.getTracks();
          setTracks(track[0]);
        })
        .catch((err) => console.log(err));
    }
  };

  const handleCapture = () => {
    const width = videoRef.current.clientWidth;
    const height = videoRef.current.clientHeight;

    canvasRef.current.setAttribute("width", width);
    canvasRef.current.setAttribute("height", height);

    const ctx = canvasRef.current.getContext("2d");
    ctx.drawImage(videoRef.current, 0, 0, width, height);
    let URL = canvasRef.current.toDataURL();
    setImages((prev) => [...prev, { camera: URL }]);
  };

  const removeImage = (index) => {
    const newArr = [...images];
    newArr.splice(index, 1);
    setImages(newArr);
  };

  const handleStopStream = () => {
    tracks.stop();
    setIsCamera((prev) => !prev);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log({ content, images });
    setLoading(true)
    console.log('images: ', images);
    const formData = new FormData();
    for (const image of images) {

      if (image.camera) {
        formData.append("images", image.camera)
      } else {
        formData.append("images", image)

      }
    }
    formData.append("content", content)

    let rs
    if (type === "create") {

      rs = await blogService.handlePostNews(formData);
    } else {
      rs = await blogService.handleUpdateNews(data?._id, formData);

    }

    if (rs?.success) {
      setLoading(false)
      toastSucess(rs?.msg)
      setIsModal(prev => !prev)
      dispatch(getAllPosts({ isCensor: true }))


    }
  };

  return (
    <form onSubmit={handleSubmit} >
      <Loading isLoading={loading} fullscreen />
      <div>
        <textarea
          className="w-full outline-none resize-none min-h-[20vh]"
          placeholder="What's news today?"
          name="content"
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />
      </div>
      <div className="grid grid-cols-4 max-h-[250px] gap-5 overflow-auto mb-5">
        {images.map((img, index) => {
          return (
            <div key={index} className="relative ">
              <img
                className=" object-cover w-[100px] h-[100px]"
                // src={img.camera ? img.camera : URL.createObjectURL(img)}
                src={img?.name ? URL.createObjectURL(img) : img}
                alt=""
              />
              <span
                onClick={() => removeImage(index)}
                className="absolute top-0 right-0 p-[3px] bg-slate-300 rounded-full cursor-pointer"
              >
                <FaTimes />
              </span>
            </div>
          );
        })}
      </div>
      {isCamera && (
        <div className="relative">
          <video
            width="100%"
            height="100%"
            ref={videoRef}
            src=""
            autoPlay
            muted
          ></video>
          <label
            onClick={handleStopStream}
            className="absolute top-0 right-0 cursor-pointer"
          >
            <FaTimes className="text-danger text-3xl" />
          </label>
          <canvas ref={canvasRef} className="hidden" />
        </div>
      )}
      {isCamera ? (
        <div className="flex items-center justify-center gap-5 mt-3">
          <label
            onClick={handleCapture}
            className="flex flex-col items-center justify-center cursor-pointer"
          >
            <AiFillCamera className="text-main text-3xl" />
          </label>
        </div>
      ) : (
        <div className="flex items-center justify-center gap-5 mt-3">
          <label className=" flex flex-col items-center justify-center cursor-pointer">
            <div className="flex flex-col items-center justify-center gap-2">
              <AiFillPicture className="text-main text-3xl" />
            </div>
            <input
              multiple
              onChange={handleChooseImage}
              type="file"
              name="images"
              accept="image/*"
              className="w-0 h-0"
            />
          </label>
          {/* <label
            onClick={handleStream}
            className=" flex flex-col items-center justify-center cursor-pointer"
          >
            <AiFillCamera className="text-main text-3xl" />
          </label> */}
        </div>
      )}
      <Button type="submit" name="Post" />
    </form>
  );
};

export default memo(FormNews);
