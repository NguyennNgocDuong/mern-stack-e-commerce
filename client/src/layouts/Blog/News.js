import React, { memo } from "react";

import NewsItem from "./NewsItem";
import { Loading } from "components";

const News = ({ data, isLoading }) => {


  return (
    <div className="col-span-4 h-[75vh] overflow-auto [&::-webkit-scrollbar]:hidden [-ms-overflow-style:'none'] [scrollbar-width:'none']">
      <Loading isLoading={isLoading}>
        {data.length > 0 ? data.map(el => {
          return <NewsItem key={el._id} data={el} />
        }) : <div className="h-full flex items-center justify-center  text-sm text-sub">Not have news</div>}
      </Loading>
      {/* {data.length > 0 ?  : <div className="h-full flex items-center justify-center  text-sm text-sub">Not have news</div>} */}


    </div>
  );
};

export default memo(News);
