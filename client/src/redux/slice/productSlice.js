import { createSlice } from '@reduxjs/toolkit'
import * as actions from "../asyncAction"

const initialState = {
    categories: null,
    isLoading: false,
}

const productSlice = createSlice({
    name: 'product',
    initialState,
    reducers: {

    },
    // Code logic xử lý async action
    extraReducers: (builder) => {
        builder.addCase(actions.getAllCategories.pending, (state) => {
            // Bật trạng thái loading
            state.isLoading = true;
        });

        builder.addCase(actions.getAllCategories.fulfilled, (state, action) => {
            state.isLoading = false;
            state.categories = action.payload;
        });

        builder.addCase(actions.getAllCategories.rejected, (state, action) => {
            state.isLoading = false;
            state.errorMessage = action.payload.message;
        });
    },
});

export const { } = productSlice.actions

export default productSlice.reducer