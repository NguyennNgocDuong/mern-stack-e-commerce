import { Skeleton } from "antd";
import React, { useEffect } from "react";
import { Loading } from "components";
import { News, SidebarBlog } from "layouts";
import { useDispatch, useSelector } from "react-redux";
import { getAllPosts } from 'redux/asyncAction';

const Blog = () => {
  const { currentUser } = useSelector((state) => state.userSlice);
  const { posts, isLoading } = useSelector((state) => state.postSlice);
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getAllPosts({ isCensor: true }))
  }, [dispatch])

  return (
    <div>
      <div className="grid grid-cols-6 gap-5">
        <SidebarBlog />
        <News data={posts} isLoading={isLoading} />
      </div>
    </div>
  );
};

export default Blog;
