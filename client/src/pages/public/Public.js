import React from 'react'
import { Outlet } from 'react-router-dom'
import { Breadcums, Footer, Loading } from 'components'
import Header from 'components/Header/Header'


const Public = () => {
    return (
        <div className="w-full flex flex-col items-center my-0 mx-auto">

            <Header />
            <div className="w-main mt-[140px] min-h-[78vh]">
                {/* <Breadcums /> */}
                <Outlet />
            </div>
            <Footer />
        </div>
    )
}

export default Public