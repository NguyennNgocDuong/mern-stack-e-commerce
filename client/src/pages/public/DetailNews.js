import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { blogService } from "services/blogService"
import Avatar from "assets/img/avatar.png";
import { formatCreatedAt } from "utils/helpers";
import { Slider } from 'components';
import { Interact, NewsItem } from 'layouts';
const DetailNews = () => {
    const [news, setNews] = useState(null)
    const { id } = useParams()

    const getCurrentNews = async (newsId) => {
        const rs = await blogService.handleGetCurrentNews(newsId)
        if (rs?.success) {
            setNews(rs?.blog)
        }
    }

    useEffect(() => {
        getCurrentNews(id)
    }, [id])



    return (
        <div className="flex items-center justify-center">
            <div className="w-[70%]">
                <NewsItem data={news} />
            </div>
        </div>
    )
}

export default DetailNews