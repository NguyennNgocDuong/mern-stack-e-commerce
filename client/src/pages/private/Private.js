import { Footer, Header } from 'components'
import React from 'react'
import { useSelector } from 'react-redux'
import { Navigate, Outlet } from 'react-router-dom'
import path from 'routes/path'

const Private = () => {
    const { isLoggedIn, currentUser } = useSelector(state => state.userSlice)

    if (!isLoggedIn || !currentUser) return <Navigate to={`/${path.HOME}`} />

    return (
        <div className="w-full flex flex-col items-center my-0 mx-auto">
            <Header />
            <div className="w-main mt-[140px] min-h-[70vh]">
                {/* <Breadcums /> */}
                <Outlet />
            </div>
            {/* <Footer /> */}
        </div>
    )
}

export default Private