import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { ListProduct, ProductItem } from "components"
import { icons } from 'utils/icons';
import { userService } from 'services/userService';
import { Link, useNavigate } from 'react-router-dom';
import { getCurrentUser } from 'redux/AsyncAction/user';
import { toastError } from 'utils/helpers';
import path from 'routes/path';
import Lottie from "lottie-react";
import empty from "assets/img/empty-cart.json";

const WishList = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { currentUser } = useSelector((state) => state.userSlice);
    const { FiTrash2 } = icons
    const handleRemoveWishList = async (id) => {
        if (currentUser) {
            const response = await userService.handleAddWishList(id)
            if (response?.success) {
                dispatch(getCurrentUser())
            }
        } else {
            toastError("Please login!");
            navigate(`/${path.LOGIN}`)
        }

    }

    return (
        <>
            <h1 className="text-xl font-semibold mb-5">Wish List</h1>
            {currentUser?.wishList?.length > 0 ? <div className="grid grid-cols-5 gap-2">
                {currentUser?.wishList?.map((product) => {
                    return <div key={product._id} className="relative">
                        <div onClick={() => handleRemoveWishList(product?._id)} className='z-50 cursor-pointer absolute top-[10px] right-[10px] bg-feature text-hover p-2 rounded-full'>
                            <FiTrash2 />
                        </div>
                        <ProductItem product={product} normal={true} />
                    </div>;
                })}
            </div> : <div className="flex flex-col justify-center items-center">
                <Lottie style={{ width: "30%" }} animationData={empty} loop={true} />
                <p className="text-sm text-sub">Not have product in wishlist</p>
                <Link
                    to={`/${path.HOME}`}
                    className="bg-feature text-hover p-2 rounded-lg cursor-pointer mt-2"
                >
                    Add now
                </Link>
            </div>}
        </>
    )
}

export default WishList