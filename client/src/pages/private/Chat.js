import { Conversation, Chatbox } from "layouts";
import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { chatService } from "services/chatService";
import { io } from "socket.io-client";

const Chat = () => {
  const { currentUser } = useSelector((state) => state.userSlice);

  const [chats, setChats] = useState([]);
  const [currentChatbox, setCurrentChatbox] = useState(null);
  const [onlineUsers, setOnlineUsers] = useState([]);
  const [sendMsg, setSendMsg] = useState(null);
  const [receiveMsg, setReceiveMsg] = useState(null);

  const socket = useRef();

  const fetchChats = async () => {
    const response = await chatService.handleGetChats();

    if (response?.success) {
      setChats(response?.data);
    }
  };

  const checkUserOnline = (chat) => {
    const memberChat = chat.members.find((el) => el !== currentUser._id)
    const online = onlineUsers.find(el => el.userId === memberChat)

    return online ? true : false
  }

  // connecting socket
  useEffect(() => {
    socket.current = io("ws://techshop-api-ha8o.onrender.com");
    socket.current.emit("new-user-add", currentUser._id);
    socket.current.on("get-users", (users) => {
      setOnlineUsers(users);
    });
  }, [currentUser]);

  //  send msg to socket
  useEffect(() => {
    if (sendMsg) {
      socket.current.emit("send-message", sendMsg);
    }
  }, [sendMsg]);

  // get msg from socket serer
  useEffect(() => {
    socket.current.on("recieve-message", (data) => {
      setReceiveMsg(data);
    });
  }, []);

  useEffect(() => {
    fetchChats();
  }, []);
  return (
    <div className=" py-3 grid grid-cols-6 gap-5">
      <div className="col-span-2 px-2 bg-sub rounded-lg">
        <h1 className="text-2xl font-semibold border-b-2 py-2">Chat</h1>
        <div className="py-2 h-[68vh] overflow-auto">
          {chats?.map((chat) => (
            <div key={chat._id} onClick={() => setCurrentChatbox(chat)}>
              <Conversation isOnline={checkUserOnline(chat)} chat={chat} currentUserId={currentUser._id} />
            </div>
          ))}
        </div>
      </div>
      <div className="col-span-4 bg-sub rounded-lg relative">
        {currentChatbox ? (
          <Chatbox

            receiveMsg={receiveMsg}
            setSendMsg={setSendMsg}
            currentChatbox={currentChatbox}
            currentUserId={currentUser._id}
          />
        ) : (
          <span className="absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] text-sub">
            Tap on chat to start
          </span>
        )}
      </div>
    </div>
  );
};

export default Chat;
