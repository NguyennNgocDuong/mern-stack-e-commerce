import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Avatar from "assets/img/avatar.png";
import { Modal, Tag } from "antd";
import { formatCreatedAt, toastSucess } from "utils/helpers";
import { useFormik } from "formik";
import * as Yup from "yup";
import { Button, Input, FormAddress } from "components";
import { userService } from "services/userService";
import { getCurrentUser } from "redux/AsyncAction/user";
import { icons } from "utils/icons";
import { on_loading, off_loading } from "redux/slice/loadingSlice";

const Profile = () => {
  const { currentUser } = useSelector((state) => state.userSlice);
  const [imageAsset, setImageAsset] = useState(currentUser?.image);
  const [isModal, setIsModal] = useState(false);
  const [type, setType] = useState(null);
  const dispatch = useDispatch();
  const { FiPlus, FiEdit } = icons;
  const formik = useFormik({
    initialValues: {
      firstName: currentUser?.firstName,
      lastName: currentUser?.lastName,
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .max(6, "Your first name must be under 6 characters!")
        .required("You must fill in this section!"),
      lastName: Yup.string()

        .max(6, "Your last name must be under 6 characters!")
        .required("You must fill in this section!"),
    }),
    onSubmit: async (values) => {
      dispatch(on_loading())
      const formData = new FormData();

      for (let i of Object.entries({ ...values, image: imageAsset })) {
        formData.append(i[0], i[1]);
      }
      const response = await userService.handleEditUser(formData);
      if (response?.success) {
        toastSucess(response?.msg);
        dispatch(off_loading())
        dispatch(getCurrentUser());
      }
    },
  });
  const addressDefault = currentUser?.address?.find(
    (el) => el?.isDefault === true
  );

  useEffect(() => {
    return () => {
      imageAsset && URL.revokeObjectURL(imageAsset.preview);
    };
  }, [imageAsset]);
  return (
    <div className="w-full grid grid-cols-6 gap-5 py-3">
      <Modal
        title={<h1 className="text-2xl ">Information receive</h1>}
        centered
        open={isModal}
        onCancel={() => setIsModal(false)}
        footer={null}
        width={650}
      >
        <FormAddress
          data={addressDefault}
          type={type}
          setIsModal={setIsModal}
        />
      </Modal>
      <div className="flex flex-col items-center">
        <img
          className="rounded-full"
          width={120}
          src={
            imageAsset?.preview
              ? imageAsset.preview
              : imageAsset
                ? `${imageAsset}`
                : Avatar
          }
          alt=""
        />

        <label className="w-full  flex flex-col items-center justify-center cursor-pointer mt-3">
          <p className=" border-2 border-main bg-sub shadow-lg px-2">Change</p>
          <input
            type="file"
            name="image"
            accept="image/*"
            onChange={(e) => {
              const file = e.target.files[0];

              file.preview = URL.createObjectURL(file);

              setImageAsset(file);
            }}
            className="w-0 h-0"
          />
        </label>
      </div>

      <div className="col-span-3 bg-sub p-3">
        <div className="flex items-center justify-between">
          <h1 className="font-semibold text-xl">Personal Information</h1>
          <Tag color={currentUser?.isBlocked ? "red" : "green"}>
            {currentUser?.isBlocked ? "Blocked" : "Active"}
          </Tag>
        </div>
        <p>Created At: {formatCreatedAt(currentUser?.createdAt)}</p>
        <form onSubmit={formik.handleSubmit}>
          <div className="flex items-center gap-5">
            <Input
              type="text"
              name="firstName"
              handleChange={formik.handleChange}
              value={formik.values.firstName}
              errors={formik.errors.firstName}
              touched={formik.touched.firstName}
            />
            <Input
              type="text"
              name="lastName"
              handleChange={formik.handleChange}
              value={formik.values.lastName}
              errors={formik.errors.lastName}
              touched={formik.touched.lastName}
            />
          </div>

          <Input
            disabled
            type="email"
            name="email"
            value={currentUser?.email}
          />
          <Button
            disabled={
              currentUser?.firstName === formik.values.firstName &&
              currentUser?.lastName === formik.values.lastName &&
              !imageAsset?.preview &&
              true
            }
            name="Save"
            type="submit"
          />
        </form>
      </div>
      <div className="col-span-2 bg-sub p-3">
        <div className="flex items-center justify-between">
          <h1 className="font-semibold text-xl">Address</h1>
          <div
            onClick={() => {
              setIsModal(true);
              setType("edit");
            }}
            className="cursor-pointer"
          >
            <FiEdit />
          </div>
        </div>
        {currentUser?.address?.length <= 0 ? (
          <div
            onClick={() => {
              setIsModal(true);
              setType("create");
            }}
            className="border text-sub flex  items-center justify-center rounded-lg cursor-pointer py-8 hover:border-2 hover:border-main "
          >
            <FiPlus className="text-2xl " />
          </div>
        ) : (
          <div>
            <Input
              disabled
              type="text"
              name="province"
              value={addressDefault.province.name}
            />
            <Input
              disabled
              type="text"
              name="district"
              value={addressDefault.district.name}
            />
            <Input
              disabled
              type="text"
              name="ward"
              value={addressDefault.ward.name}
            />
            <Input
              disabled
              type="text"
              name="street"
              value={addressDefault.street}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default Profile;
