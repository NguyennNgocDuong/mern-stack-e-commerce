import { Modal, Tag } from "antd";
import { DetailOrder, FormSelect, TableComponent } from "components";
import React, { useCallback, useEffect, useState } from "react";
import { createSearchParams, useNavigate, useSearchParams } from "react-router-dom";
import { orderService } from "services/orderService";
import { statusOrder } from "utils/constants";
import { formatCreatedAt } from "utils/helpers";
import Lottie from "lottie-react";
import empty from "assets/img/empty-cart.json";
import { showDeleteConfirm, toastSucess } from "utils/helpers";


const Order = () => {
  const [params] = useSearchParams();
  const [orders, setOrder] = useState([]);
  const [status, setStatus] = useState("processing");
  const [isModal, setIsModal] = useState(false)
  const [currentOrder, setCurrentOrder] = useState(null)
  const navigate = useNavigate()
  const columns = [
    {
      title: "ID",
      dataIndex: "_id",
      render: (text, row) => {

        const handleViewDetail = (row) => {
          setCurrentOrder(row)

          setIsModal(true)
        }


        return <div onClick={() => handleViewDetail(row)}
          className="cursor-pointer hover:text-main">{text}</div>
      },
    },
    {
      title: "Date of purchase",
      dataIndex: "createdAt",
      render: (text) => <p>{formatCreatedAt(text)}</p>,
    },
    {
      title: "Total",
      dataIndex: "total",
      render: (text) => <p >${text}</p>,
    },
    {
      title: "Status",
      dataIndex: "status",

      render: (text) => {
        let color;
        if (text === "processing") color = "orange";
        if (text === "delivered") color = "green";
        if (text === "canceled") color = "red";
        return <Tag color={color}>{text.toUpperCase()}</Tag>;
      },

    },

    {
      title: "Action",
      dataIndex: "",
      hidden: status !== "processing" && true,
      render: (_, record) => {


        const handleCancelOrder = async () => {
          const response = await orderService.handleCancelOrder(record?._id)
          if (response?.success) {
            toastSucess(response?.msg)
            fetchOrder({ sort: "-createdAt", status });

          }

        }

        return <div onClick={() => showDeleteConfirm(`this order`, handleCancelOrder)} className="bg-danger text-sm w-[50%] text-center rounded-lg text-hover p-1 cursor-pointer">Cancel</div>;
      },
    },
  ].filter(item => !item.hidden);

  useEffect(() => {

    const param = [];

    for (let i of params.entries()) param.push(i);
    const queries = {};

    for (let i of param) queries[i[0]] = i[1];
    queries.status = status
    // queries.page = 1

    navigate({
      search: createSearchParams(queries).toString(),
    });

    fetchOrder({ ...queries, sort: "-createdAt" })


  }, [status, params]);

  const fetchOrder = useCallback(
    async (queries) => {
      const response = await orderService.handleGetOrderUser(queries);
      if (response?.success) {
        setOrder(response);
      }
    },
    [status],
  )




  useEffect(() => {

    fetchOrder({ sort: "-createdAt", status });
  }, []);

  return (
    <div className="w-full">
      <Modal
        title={<h1 className="text-2xl ">Detail Order</h1>}
        centered
        open={isModal}
        onCancel={() => setIsModal(false)}
        footer={null}
        width={1000}
      >
        <DetailOrder status={status} data={currentOrder} />
      </Modal>
      <div className="flex items-center justify-between">
        <h1 className="font-semibold text-xl">My order</h1>
        <FormSelect className="cursor-pointer p-2 outline-none" options={statusOrder} value={status} setValue={(e) => setStatus(e.target.value)} />
      </div>

      {orders?.orders?.length > 0 ? <TableComponent columns={columns} data={orders.orders}
        total={orders?.count}
        fechData={fetchOrder}
        pageSize={5}
        queries={{ status }}
      /> : <div className="flex flex-col justify-center items-center">
        <Lottie style={{ width: "30%" }} animationData={empty} loop={true} />

      </div>}

    </div>
  );
};

export default Order;
