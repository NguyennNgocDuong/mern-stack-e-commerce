
import { Button, FormCategory, FormSelect, TableComponent } from 'components'
import React, { useCallback, useEffect, useState } from 'react'
import { icons } from "utils/icons"
import { categoryService } from "services/categoryService"
import { formatCreatedAt, showDeleteConfirm, toastSucess } from 'utils/helpers'
import { useSearchParams } from 'react-router-dom'
import { Modal } from 'antd'

const ManageCategory = () => {
    const { FiPlus, FiEdit, FiTrash2 } = icons
    const [params] = useSearchParams();


    const [categoryData, setCategoryData] = useState(null)
    const [isModal, setIsModal] = useState(false)
    const [type, setType] = useState("")
    const [currentCategory, setCurrentCategory] = useState(null)


    const columns = [
        {
            title: "Name",
            dataIndex: "title",
            //   filteredValue: [valueSearch],

            // onFilter: (value, record) => {
            //     return record.product_name.toLowerCase().includes(value.toLowerCase());
            // },
        },
        {
            title: "Image",
            dataIndex: "image",
            render: (text) => (

                <img width={70} src={text} alt="" />
            ),
        },

        {
            title: "Brand",
            dataIndex: "brand",
            render: (text) => (
                text?.map((el) => (
                    <span className="bg-feature text-hover px-2 py-1 mr-2 rounded" key={el}>{el.toUpperCase()}</span>
                ))

            ),
        },
        {
            title: "Created At",
            dataIndex: "createdAt",
            render: (text) => <p>{formatCreatedAt(text)}</p>,


        },
        {
            title: "Action",
            dataIndex: "action",
            render: (_, record) => {
                return renderAction(record);
            },
        },
    ];

    const renderAction = (record) => {
        const handleEditFood = () => {
            setIsModal(true)
            setType("edit");
            setCurrentCategory(record)


        };

        const handleDeleteCategory = async () => {
            const response = await categoryService.handleDeleteCategory(record._id);
            if (response?.success) {
                toastSucess(response?.msg)
                handleGetAllCategories({ sort: "-createdAt" })

            }
        }

        return (
            <div className="flex">
                <FiEdit
                    onClick={() => handleEditFood(record)}
                    style={{ color: "green", fontSize: "25px", cursor: "pointer" }}
                />
                <FiTrash2
                    onClick={() => showDeleteConfirm(`${record?.title}`, handleDeleteCategory)}
                    style={{ color: "red", fontSize: "25px", cursor: "pointer" }}
                />
            </div>
        );
    };

    const handleGetAllCategories = useCallback(async (queries) => {
        const response = await categoryService.handleGetAllCategories(queries)
        setCategoryData(response)
    }, [])

    useEffect(() => {
        const param = [];

        for (let i of params.entries()) param.push(i);
        const queries = { sort: "-createdAt" };

        for (let i of param) queries[i[0]] = i[1];
        handleGetAllCategories(queries)

    }, [params])

    useEffect(() => {
        !isModal && setCurrentCategory(null)

    }, [isModal])


    return (
        <div className="px-[50px] py-5">
            <Modal
                title={type === "create" ? "Create Product" : "Edit Product"}
                centered
                open={isModal}
                onCancel={() => setIsModal(false)}
                footer={null}
                width={1000}

            >
                <FormCategory setIsModal={setIsModal} type={type} data={currentCategory} handleGetAllCategories={handleGetAllCategories} />
            </Modal>
            <div className="w-full flex items-center justify-between">



                <h1 className="text-2xl font-semibold">Product Management</h1>
                <div className="flex">
                    {/* <FormSelect
                        type="category"
                        // options={listCategory}
                        // setValue={setValueFilterCategory}
                        className="p-2 mr-5"
                        isLabel={false}
                        label="category"
                    /> */}
                    <Button
                        handleOnclick={() => {
                            setIsModal(true);
                            setType("create");
                        }}
                        className="flex items-center bg-feature text-white p-2 hover:bg-blue-500 transition-all duration-100 ease-in-out"
                        name={<FiPlus />}
                    />


                </div>
            </div>

            <TableComponent
                columns={columns}
                data={categoryData?.categories}
                total={categoryData?.count}
                pageSize={categoryData?.categories?.length}
                fechData={handleGetAllCategories}
            />

        </div>
    )
}

export default ManageCategory