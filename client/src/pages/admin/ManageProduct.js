import { Button, FormProduct, FormSelect, PaginationComponent, TableComponent } from 'components'
import React, { useCallback, useEffect, useState } from 'react'
import { icons } from "utils/icons"
import { productService } from "services/productService"
import { formatCreatedAt, showDeleteConfirm, toastSucess } from 'utils/helpers'
import { useSearchParams } from 'react-router-dom'
import { Modal, Tag } from 'antd'

const ManageProduct = () => {
    const { FiPlus, FiEdit, FiTrash2 } = icons
    const [params] = useSearchParams();
    const [productData, setProductData] = useState(null)
    const [isModal, setIsModal] = useState(false)
    const [type, setType] = useState("")
    const [currentProduct, setCurrentProduct] = useState(null)


    const columns = [
        {
            title: "Name",
            dataIndex: "title",
            //   filteredValue: [valueSearch],

            onFilter: (value, record) => {
                return record.product_name.toLowerCase().includes(value.toLowerCase());
            },
        },
        {
            title: "Image",
            dataIndex: "thump",
            render: (text) => (
                <img width={70} src={text} alt="" />
            ),
        },
        {
            title: "Color",
            dataIndex: "color",
            render: (text) => (
                <div className="grid grid-cols-5">
                    {text?.map((el) => (
                        <Tag color={el.toLowerCase()} key={el}>
                            {el}
                        </Tag>
                    ))}
                </div>

            ),
        },
        {
            title: "Quantity",
            dataIndex: "quantity",
            sorter: (a, b) => a.quantity - b.quantity,


        },
        {
            title: "Sold",
            dataIndex: "sold",
            sorter: (a, b) => a.sold - b.sold,


        },
        {
            title: "Price",
            dataIndex: "price",
            sorter: (a, b) => a.price - b.price,

            render: (text) => <p>${text}</p>,
        },

        {
            title: "Category",
            // dataIndex: ["category", "category_name"],
            dataIndex: "category",

        },
        {
            title: "Brand",
            dataIndex: "brand",

        },
        {
            title: "Created At",
            dataIndex: "createdAt",
            render: (text) => <p>{formatCreatedAt(text)}</p>,


        },
        {
            title: "Action",
            dataIndex: "action",
            render: (_, record) => {
                return renderAction(record);
            },
        },
    ];

    const renderAction = (record) => {
        const handleEditFood = () => {
            setIsModal(true)
            setType("edit");
            setCurrentProduct(record)


        };

        const handleDeleteProduct = async () => {
            const response = await productService.handleDeleteProduct(record._id);
            if (response?.success) {
                toastSucess(response?.msg)
                handleGetAllProducts({ sort: "-createdAt" })

            }
        }

        return (
            <div className="flex">
                <FiEdit
                    onClick={() => handleEditFood(record)}
                    style={{ color: "green", fontSize: "25px", cursor: "pointer" }}
                />
                <FiTrash2
                    onClick={() => showDeleteConfirm(`${record?.title}`, handleDeleteProduct)}
                    style={{ color: "red", fontSize: "25px", cursor: "pointer" }}
                />
            </div>
        );
    };

    const handleGetAllProducts = useCallback(async (queries) => {
        const response = await productService.handleGetAllProducts(queries)
        setProductData(response)
    }, [])

    useEffect(() => {
        const param = [];

        for (let i of params.entries()) param.push(i);
        const queries = { sort: "-createdAt" };

        for (let i of param) queries[i[0]] = i[1];
        handleGetAllProducts(queries)

    }, [params])

    useEffect(() => {
        !isModal && setCurrentProduct(null)

    }, [isModal])


    return (
        <div className="px-[50px] py-5">
            <Modal
                title={type === "create" ? "Create Product" : "Edit Product"}
                centered
                open={isModal}
                onCancel={() => setIsModal(false)}
                footer={null}
                width={1000}

            >
                <FormProduct setIsModal={setIsModal} type={type} data={currentProduct} handleGetAllProducts={handleGetAllProducts} />
            </Modal>
            <div className="w-full flex items-center justify-between">



                <h1 className="text-2xl font-semibold">Product Management</h1>
                <div className="flex">
                    {/* <FormSelect
                        type="category"
                        // options={listCategory}
                        // setValue={setValueFilterCategory}
                        className="p-2 mr-5"
                        isLabel={false}
                        label="category"
                    /> */}
                    <Button
                        handleOnclick={() => {
                            setIsModal(true);
                            setType("create");
                        }}
                        className="flex items-center bg-feature text-white p-2 hover:bg-blue-500 transition-all duration-100 ease-in-out"
                        name={<FiPlus />}
                    />


                </div>
            </div>

            <TableComponent
                columns={columns}
                data={productData?.products}
                total={productData?.count}
                fechData={handleGetAllProducts}
            />

        </div>
    )
}

export default ManageProduct