import { FormUser, TableComponent } from 'components';
import React, { useEffect, useState } from 'react'
import { userService } from 'services/userService'
import { Tag } from "antd";
import { formatCreatedAt, showDeleteConfirm, toastSucess } from 'utils/helpers';
import { icons } from 'utils/icons';
import { Modal } from 'antd';

const ManageUser = () => {
    const [userData, setUserData] = useState(null)
    const [currentUser, setCurrentUser] = useState(null)
    const [isModal, setIsModal] = useState(false)
    const { FiEdit, FiTrash2 } = icons
    const queries = { sort: "-createdAt" }

    const columns = [
        {
            title: "First Name",
            dataIndex: "firstName",
        },
        {
            title: "Last Name",
            dataIndex: "lastName",
        },
        {
            title: "Email",
            dataIndex: "email",
            //   filteredValue: [valueSearch],
            //   onFilter: (value, record) => {
            //     return record.email.toLowerCase().includes(value.toLowerCase());
            //   },
        },


        {
            title: "Created At",
            dataIndex: "createdAt",
            render: (text) => <p>{formatCreatedAt(text)}</p>,
        },
        {
            title: "Status",
            dataIndex: "isBlocked",
            render: (_, { isBlocked }) => {
                return <Tag color={isBlocked ? "red" : "green"} key={isBlocked}>
                    {isBlocked ? "BLOCKED" : "ACTIVE"}
                </Tag>
            },
        },
        {
            title: "Role",
            dataIndex: "role",
            render: (_, { role }) => {
                return <Tag color={role === "admin" ? "blue" : "green"} key={role}>
                    {role.toUpperCase()}
                </Tag>
            },

        },
        {
            title: "Action",
            dataIndex: "action",
            render: (_, record) => {
                return renderAction(record);
            },
        },
    ];

    const renderAction = (record) => {


        const handleEditUser = () => {
            setIsModal(true)
            setCurrentUser(record)
        }

        const handleDeleteUser = async () => {
            const response = await userService.handleDeleteUser(record?._id)
            if (response?.success) {
                toastSucess(response?.msg)
                fetchAllUsers(queries)

            }

        }


        return (
            <div className="flex">

                <FiEdit
                    onClick={handleEditUser}
                    style={{ color: "green", fontSize: "25px", cursor: "pointer" }}
                />
                <FiTrash2
                    onClick={() => showDeleteConfirm(`${record?.email}`, handleDeleteUser)}
                    style={{ color: "red", fontSize: "25px", cursor: "pointer" }}
                />
            </div>
        );
    };
    const fetchAllUsers = async (queries) => {
        const response = await userService.handleGetAllUsers(queries)
        setUserData(response)
    }
    useEffect(() => {


        fetchAllUsers(queries)
    }, [])

    useEffect(() => {
        !isModal && setCurrentUser(null)

    }, [isModal])



    return (
        <div className='px-[50px]'>

            <Modal
                title="Edit User"
                centered
                open={isModal}
                onCancel={() => setIsModal(false)}
                footer={null}

            >

                <FormUser fetchAllUsers={fetchAllUsers} setModal={setIsModal} data={currentUser} />
            </Modal>
            <TableComponent columns={columns} data={userData?.users} total={userData?.count}
                fechData={fetchAllUsers} />
        </div>
    )
}

export default ManageUser