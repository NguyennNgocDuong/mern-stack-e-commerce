import { AdminSidebar, HeaderAdmin } from 'components'
import React from 'react'
import { useSelector } from 'react-redux'
import { Navigate, Outlet } from 'react-router-dom'
import path from 'routes/path'

const Admin = () => {
    const { isLoggedIn, currentUser } = useSelector(state => state.userSlice)

    if (!isLoggedIn || !currentUser || currentUser?.role !== 'admin') return <Navigate to={`/${path.HOME}`} />

    return (
        <div className='flex'>
            <AdminSidebar />
            <div className='w-full'>
                <HeaderAdmin />
                <Outlet />
            </div>
        </div>
    )
}

export default Admin