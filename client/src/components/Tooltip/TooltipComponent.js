import React, { memo } from 'react'
import { Tooltip } from 'antd';

const TooltipComponent = ({ children, placement, title }) => {
    return (
        <Tooltip placement={placement} title={title}>
            {children}

        </Tooltip>
    )
}

export default memo(TooltipComponent)