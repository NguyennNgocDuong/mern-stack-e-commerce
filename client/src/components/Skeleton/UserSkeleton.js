import React from "react";

const UserSkeleton = ({ isLoading, children }) => {
  return (
    <>
      {isLoading ? (
        <div className="flex items-center gap-5 mb-5">
          <div className="w-[40px] h-[40px] bg-slate-300 rounded-full"></div>
          <div className="flex flex-col justify-center gap-1">
            <div className="w-56 h-[15px] bg-slate-300 rounded-lg"></div>
            <div className="w-32 h-[15px] bg-slate-300 rounded-lg"></div>
          </div>
        </div>
      ) : (
        children
      )}
    </>
  );
};

export default UserSkeleton;
