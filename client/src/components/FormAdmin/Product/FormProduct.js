import { useFormik } from "formik";
import * as Yup from "yup";

import React, { memo, useCallback, useEffect, useState } from "react";
import Input from "components/Input/Input";
import FormSelect from "components/FormSelect/FormSelect";
import { useSelector } from "react-redux";
import { productService } from "services/productService";
import Button from "components/Button/Button";
import { icons } from "utils/icons";
import Markdown from "components/Markdown/Markdown";
import { toastError, toastSucess } from "utils/helpers";
import FormCheckbox from "components/FormCheckbox/FormCheckbox";
import { colors } from "utils/constants";
import { categoryService } from "services/categoryService";



const FormProduct = ({
    setIsModal,
    data,
    handleGetAllProducts,
    type,
}) => {
    const [imageAsset, setImageAsset] = useState(null);
    const [markdown, setMarkdown] = useState("")
    const [categories, setCategories] = useState([]);

    const { MdCloudUpload, MdDelete, MdKeyboardArrowDown } = icons
    const [selected, setSelected] = useState([]);

    const formik = useFormik({
        initialValues: {
            title: data ? data.title : "",
            category: data ? data.category : "",
            brand: data ? data.brand : "",

            thump: data ? data.thump : imageAsset,
            quantity: data ? data.quantity : "",
            price: data ? data.price : "",
            description: data ? data.description : markdown,
        },
        validationSchema: Yup.object({
            title: Yup.string().required("You must fill in this section!"),
            category: Yup.string().required("You must fill in this section!"),
            brand: Yup.string().required("You must fill in this section!"),
            thump: Yup.string().required("You must choose image!"),
            quantity: Yup.number().required("You must fill in this section!"),
            price: Yup.number().required("You must fill in this section!"),
            description: Yup.string().required("You must fill in this section!"),
        }),
        enableReinitialize: true,
        onSubmit: async (values) => {
            const formData = new FormData();

            for (let i of Object.entries({ ...values, color: selected })) {

                formData.append(i[0], i[1])
            }

            setIsModal(false);

            try {
                let res;
                if (type === "create") {
                    res = await productService.handleCreateProduct(formData);
                } else {

                    res = await productService.handleUpdateProduct(data?._id, formData);
                }

                if (res?.success) {
                    toastSucess(res?.msg)
                    handleGetAllProducts({ sort: "-createdAt" })
                } else {
                    toastError(res?.msg)
                }
            } catch (error) {
                console.log("error: ", error);
            }
        },
    });
    const brands = categories?.find((el) => el.title === formik.values.category)?.brand?.map(el => ({ id: el, title: el }))
    const fetchCategories = async () => {
        const response = await categoryService.handleGetAllCategories();
        setCategories(response.categories);
    };

    useEffect(() => {
        fetchCategories();
    }, []);
    const handleChangeMarkdown = useCallback((e) => {
        setMarkdown(e.target.getContent());
    }, [])
    const handleSelected = (e) => {
        const { checked, value } = e.target;
        if (checked) {
            setSelected((prev) => [...prev, value]);
        } else {
            setSelected((prev) => [...prev.filter((el) => el !== value)]);
        }
    };

    useEffect(() => {
        data && setImageAsset(data.thump);
        return () => {
            setImageAsset(null);
        };
    }, [data]);

    useEffect(() => {
        return () => {
            imageAsset && URL.revokeObjectURL(imageAsset.preview);
        };
    }, [imageAsset]);

    useEffect(() => {


        return () => {
            setMarkdown("")
        }
    }, [])


    return (
        <div className="w-full mt-5">
            <div className="w-full flex justify-center">
                <div className="w-full my-3 text-xl text-gray-900 font-semibold">
                    <form
                        onSubmit={formik.handleSubmit}
                        className="w-full  border border-sub rounded-lg p-4 flex flex-col  gap-4"
                    >
                        <Input
                            type="text"
                            name="title"
                            handleChange={formik.handleChange}
                            value={formik.values.title}
                            errors={formik.errors.title}
                            touched={formik.touched.title}
                        />
                        <div className="w-full grid grid-cols-3 gap-3">




                            <Input
                                type="number"
                                name="quantity"
                                handleChange={formik.handleChange}
                                value={formik.values.quantity}
                                errors={formik.errors.quantity}
                                touched={formik.touched.quantity}
                            />




                            <Input
                                type="number"
                                name="price"
                                handleChange={formik.handleChange}
                                value={formik.values.price}
                                errors={formik.errors.price}
                                touched={formik.touched.price}
                            />



                            <div className="flex items-center justify-between cursor-pointer px-3 py-2 border-2 rounded-lg border-sub relative mt-5 group  ease-in duration-300 after:absolute after:right-0 after:bottom-[-15px] after:w-full after:h-[20px]">

                                <p>Color</p>

                                <MdKeyboardArrowDown
                                    className="rotate-[270deg] group-hover:rotate-[360deg] ease-in duration-300"
                                // style={{ transform: "rotate(270deg)" }} 
                                />



                                <div className="z-50 w-full px-3  hidden group-hover:grid grid-cols-3 absolute  border right-[0px] top-[50px] bg-white rounded-lg shadow-lg ">


                                    <FormCheckbox
                                        // errors={formik.errors.brand}
                                        // touched={formik.touched.brand}
                                        data={colors} onChange={handleSelected} />

                                </div>


                            </div>

                        </div>


                        <div className="w-full grid grid-cols-2 gap-3">
                            <div className="w-full">
                                <FormSelect name="category" value={formik.values.category} options={categories} choose="Category" className="outline-none w-full text-base border-b-2 border-sub p-2 rounded-md cursor-pointer" errors={formik.errors.category} touched={formik.touched.category} handleChange={formik.handleChange} valueTitle={true} />
                            </div>
                            <div className="w-full">
                                <FormSelect name="brand" value={formik.values.brand} options={brands} choose="Brand" className="outline-none w-full text-base border-b-2 border-sub p-2 rounded-md cursor-pointer" errors={formik.errors.brand} touched={formik.touched.brand} handleChange={formik.handleChange} valueTitle={true} />
                            </div>
                        </div>


                        <div className="group flex justify-center items-center flex-col border-2 border-dotted border-sub w-full h-225 md:h-[30vh] cursor-pointer rounded-lg">
                            <>
                                {!imageAsset ? (
                                    <>
                                        <label className="w-full h-full flex flex-col items-center justify-center cursor-pointer">
                                            <div className="w-full h-full flex flex-col items-center justify-center gap-2">
                                                <MdCloudUpload className="text-gray-500 text-3xl hover:text-gray-700" />
                                                <p className="text-gray-500 hover:text-gray-700">
                                                    Click here to upload
                                                </p>
                                            </div>
                                            <input
                                                type="file"
                                                name="thump"
                                                accept="image/*"
                                                onChange={(e) => {
                                                    const file = e.target.files[0];
                                                    file.preview = URL.createObjectURL(file);

                                                    setImageAsset(file);

                                                    return formik.setFieldValue(
                                                        "thump",
                                                        e.target.files[0]
                                                    );
                                                }}
                                                className="w-0 h-0"
                                            />
                                        </label>
                                    </>
                                ) : (
                                    <>
                                        <div className="relative h-full">
                                            <img
                                                src={
                                                    imageAsset?.preview
                                                        ? imageAsset.preview
                                                        : `${imageAsset}`
                                                }
                                                alt="uploaded image"
                                                className="w-full h-full object-cover"
                                            />
                                            <button
                                                type="button"
                                                className="absolute bottom-3 right-3 p-3 rounded-full bg-red-500 text-xl cursor-pointer outline-none hover:shadow-md  duration-500 transition-all ease-in-out"
                                                onClick={() => setImageAsset(null)}
                                            >
                                                <MdDelete className="text-white" />
                                            </button>
                                        </div>
                                    </>
                                )}
                            </>
                        </div>
                        {formik.errors.thump && formik.touched.thump && (
                            <p className="text-red-500 text-sm italic">
                                {formik.errors.thump}
                            </p>
                        )}
                        <div className=" border-2 border-sub ">
                            <Markdown name="description"
                                handleChange={handleChangeMarkdown}
                                value={formik.values.description}
                                errors={formik.errors.description}
                                touched={formik.touched.description} />
                        </div >
                        <div className="flex items-center w-full">
                            <Button name={type === "create" ? "Create" : "Save"} type="submit" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default memo(FormProduct);
