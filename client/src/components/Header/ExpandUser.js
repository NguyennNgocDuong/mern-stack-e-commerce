import React, { memo } from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { logout } from "redux/slice/userSlice";
import path from "routes/path";
import { icons } from "utils/icons";

const ExpandUser = ({ currentUser }) => {
  const {
    BsChatDots,
    BsHeart,
    MdLogout,
    MdManageAccounts,
    MdPerson,
    MdReorder,
  } = icons;
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const logOut = () => {
    dispatch(logout());
    navigate(`/${path.LOGIN}`);
  };

  const navlink = [
    {
      id: 1,
      name: "Profile",
      icon: <MdPerson style={{ fontSize: "20px" }} />,
      path: `/${path.PRIVATE}/${path.PROFILE}`,
    },
    {
      id: 2,
      name: "My Order",
      icon: <MdReorder style={{ fontSize: "20px" }} />,
      path: `/${path.PRIVATE}/${path.ORDER}`,
    },
    {
      id: 3,
      name: "Wish List",
      icon: <BsHeart style={{ fontSize: "20px" }} />,
      path: `/${path.PRIVATE}/${path.WISHLIST}`,
    },
    {
      id: 4,
      name: "Message",
      icon: <BsChatDots style={{ fontSize: "20px" }} />,
      path: `/${path.PRIVATE}/${path.CHAT}`,
    },
  ];

  return (
    <>
      {currentUser?.role === "admin" && (
        <Link
          to={`/${path.ADMIN}/${path.DASHBOARD}`}
          className="flex items-center cursor-pointer text-sm  gap-3.5 font-medium p-3  hover:bg-gray-300 transition-all ease-in-out duration-300"
        >
          <MdManageAccounts style={{ fontSize: "20px" }} /> Manage
        </Link>
      )}

      {navlink.map((el) => {
        return (
          <Link
            key={el.name}
            to={el.path}
            className="flex items-center cursor-pointer text-sm  gap-3.5 font-medium p-3 hover:bg-gray-300 transition-all ease-in-out duration-300"
          >
            {el.icon} {el.name}
          </Link>
        );
      })}

      <p
        onClick={logOut}
        className="flex items-center cursor-pointer text-sm  gap-3.5 font-medium p-3  hover:bg-gray-300 transition-all ease-in-out duration-300"
      >
        <MdLogout style={{ fontSize: "20px" }} /> Log Out
      </p>
    </>
  );
};

export default memo(ExpandUser);
