const path = {
    PUBLIC: "/",
    ALL: "*",
    HOME: "",
    LOGIN: "login",
    REGISTER: "register",
    FORGOTPASSWORD: "forgot-password",
    PRODUCT: "products/:category",
    BLOG: "news",
    CONTACT: "contact",
    WISHLIST: "wishlist",
    CART: "cart",
    CHECKOUT: "checkout",
    DETAIL_PRODUCT: ":category/:id/:title",
    DETAIL_NEWS: "news/:id",

    // private
    PRIVATE: "private",
    PROFILE: "profile",
    ORDER: "order",
    CHAT: "chat",

    // Admin
    ADMIN: "admin",
    DASHBOARD: "dashboard",
    MANAGEPRODUCT: "manage-product",
    MANAGEUSER: "manage-user",
    MANAGECATEGORY: "manage-category",
    MANAGEBRAND: "manage-brand",
    MANAGEORDER: "manage-order",

}

export default path